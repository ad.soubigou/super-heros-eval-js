"use strict";

const selectHero = document.getElementById("superHerosName");
const selectPublisher = document.getElementById("superHerosPublisher");
const displayResults = document.getElementById("display");
const loader = document.querySelector(".loader");
const modal = document.getElementById("modal");
const modalContent = document.getElementById("modal-content");
const arrHeroes = [];
const arrPublishers = [];

// Fonction qui permet de créer de nouveaux éléments HTML.
function createNode(element) {
  return document.createElement(element);
}

// Fonction qui permet de remplir les nouveaux éléments HTML créés.
function append(parent, el) {
  return parent.appendChild(el);
}

// Fonction qui permet d'enlever les doublons dans la liste "select".
function testDouble(arrayString, string) {
  if (string !== "" && string !== null && arrayString.indexOf(string) === -1) {
    arrayString.push(string);
  }
  return arrayString.sort();
}

// Fonction qui permet de créer les "cards".
function createCards(hero) {
  // Création des éléments HTML qui constitueront les "cards".
  let cardHero = createNode("div");
  let imgHero = createNode("img");
  let cardBody = createNode("div");
  let cardText = createNode("h2");
  // Ajout des classes Bootstrap aux "cards".
  cardHero.classList.add("card");
  imgHero.classList.add("card-img-top");
  cardBody.classList.add("card-body");
  // Ajout du contenu aux "cards".
  imgHero.src = hero.images.lg;
  cardText.textContent = hero.name;
  // Ajout des cards à la page.
  append(displayResults, cardHero);
  append(cardHero, imgHero);
  append(cardHero, cardBody);
  append(cardBody, cardText);
}

//Fonction qui permet de créer les "options", d'ajouter le contenu et de les insérer dans le select.
function createOptions(array, selectElement) {
  // Parcours des tableaux qui vont contenir respectivement tous les noms de héros et de publishers.
  array.forEach((element) => {
    // Création des éléments HTML "option".
    let option = createNode("option");
    // Ajout du contenu aux options.
    option.value = element;
    option.textContent = element;
    // Insertion des "options" dans leurs "select" respectifs.
    append(selectElement, option);
  });
}

// Fonction principale qui va permettre de parcourir le .json et générer les événements sur la page web en appelant les différentes fonctions créées précédemment.
function displayData() {
  // Affichage du "loader" au début du chargement des données (https://www.w3schools.com/jsref/prop_style_display.asp).
  loader.style.display = "block";

  // Méthode fetch qui permet d'envoyer une requête et renvoie une promesse qui lorsqu'elle est résolue donne accès aux informations contenues dans l'objet requêté. Par défaut, fetch utilise la méthode GET pour requêter (https://fr.javascript.info/fetch).
  fetch("./superHeros.json")
    // On retourne la promesse renvoyée au format json
    .then((response) => response.json())
    .then((data) => {
      // Récupération des données du .json dans un tableau.
      const heroes = data.supersHeros;
      // Parcours de chaque cellule du tableau heroes représentant le json.
      heroes.forEach((hero) => {
        // Appel de la fonction pour créer et afficher les "cards" après le chargement de la page.
        createCards(hero);
        // Appel de la fonction permettant de remplir deux tableaux contenant respectivement la liste des noms des héros et des publishers, sans doublons et triés par ordre alphabétique.
        testDouble(arrHeroes, hero.name);
        testDouble(arrPublishers, hero.biography.publisher);
      });

      // Appel de la fonction pour la création des "options" et ajout du contenu.
      createOptions(arrHeroes, selectHero);
      createOptions(arrPublishers, selectPublisher);

      // ----------------Evènements sur les éditeurs------------------//

      // Ajout d'une écoute sur le "select" des éditeurs qui est déclenchée à chaque changement de valeur du "select" (https://developer.mozilla.org/fr/docs/Web/API/HTMLElement/change_event).
      selectPublisher.addEventListener("change", () => {
        // Récupération de la valeur de mon "select" égale à l' "option" séléctionnée.
        const selectedPublisher = selectPublisher.value;
        // Désactiver l'autre select en cliquant sur une option du select "actif" (https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/disabled).
        // Utilisation de l'opérateur ternaire (https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Conditional_operator).
        selectedPublisher === "all"
          ? (selectHero.disabled = false)
          : (selectHero.disabled = true);

        // Suppression des éléments précédemment afficher à chaque click sur une "option".
        displayResults.innerHTML = "";
        heroes.forEach((hero) => {
          // Condition permettant l'affichage ou non des "cards" selon l' "option" sélectionnée.
          if (
            selectedPublisher === "all" ||
            hero.biography.publisher === selectedPublisher
          ) {
            createCards(hero);
          }
        });
      });

      // -----------------Evènements sur les héros--------------------//

      // Ecoute sur le "select" des super-héros.
      selectHero.addEventListener("change", () => {
        const selectedHero = selectHero.value;
        selectedHero === "all"
          ? (selectPublisher.disabled = false)
          : (selectPublisher.disabled = true);

        displayResults.innerHTML = "";

        heroes.forEach((hero) => {
          // Condition pour l'ajout de contenu à la modale selon l' "option" sélectionnée.
          if (hero.name === selectedHero) {
            // Ajout du contenu à la "modale".
            modalContent.innerHTML = `              
            <img src="${hero.images.lg}">
            <div>
              <h3>${hero.name.toUpperCase()}</h3>
              <span>intelligence : ${hero.powerstats.intelligence}</span>
              <span>strength : ${hero.powerstats.strength}</span>
              <span>speed : ${hero.powerstats.speed}</span>
              <span>durability : ${hero.powerstats.durability}</span>
              <span>power : ${hero.powerstats.power}</span>
              <span>combat : ${hero.powerstats.combat}</span>
            </div>`;
            // Affichage de la modale en passant le "display : none" défini dans le css à "display : block".
            modal.style.display = "block";
          }
          // Affichage de toutes les cards lorsqu'on clique sur "all".
          else if (selectedHero === "all") {
            createCards(hero);
          }
          // Ajout d'une écoute à l'objet window représentant la fenêtre contenant le DOM (https://developer.mozilla.org/fr/docs/Web/API/Window). Ici, le container de la modale prend 100% de la fenêtre ce qui permet de faire disparaître la modale lorsqu'on clique en dehors de son contenu (https://developer.mozilla.org/fr/docs/Web/API/Event/target).
          window.addEventListener("click", (event) => {
            if (event.target === modal) {
              modal.style.display = "none";
            }
          });
        });
      });
      // Disparition du "loader" à la fin du chargement de la page.
      loader.style.display = "none";
    })
    .catch((error) => {
      loader.style.display = "block";
      console.log(error);
    });
}

displayData();
